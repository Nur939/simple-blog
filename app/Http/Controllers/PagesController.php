<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    //
    public function index()
    {
        $title = "Welcome to Laravel";
        return view("Pages.index",compact("title"));
    }

    public function about()
    {
        $title = "About Laravel";
        return view("Pages.about")->with("title",$title);
    }

    public function services()
    {
        $data = array(
            "title" =>  "Our Services",
            "services"  =>  ["Laravel","WordPress","PHP","Bootstrap","javaScript"]
        );
        return view("Pages.services")->with($data);
    }
}
