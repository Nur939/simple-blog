@extends("layouts.app")

@section("content")
    <h1 class = "text-center">{{$title}}</h1>
    <p>
        @if(count($services) > 0)
            <ul class = "list-group">
                @foreach($services as $service)
                    <li class = "list-group-item text-center">{{$service}}</li>
                @endforeach
            </ul>
        @else
            No Services    
        @endif
    </p>
@endsection