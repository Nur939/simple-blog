@extends("layouts.app")

@section("content")
    <h1 class = "text-center">{{$title}}</h1>
    <p class = "text-justify">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos saepe animi recusandae non autem quasi ad atque exercitationem adipisci eveniet quo alias nobis id, distinctio cumque est quam asperiores voluptatibus aliquid consectetur vero in. Nihil, dolorum quas! Omnis, iste deleniti ipsam explicabo possimus placeat officia quia, esse nemo dolor tempore earum quisquam sequi maiores! Architecto dolor perspiciatis nulla ipsa dolore sunt officiis unde soluta rerum cum voluptatibus voluptates numquam consectetur quis veritatis dignissimos commodi optio, laudantium provident expedita illo officia neque facere hic. Beatae voluptatem, incidunt dolor quia eum laborum odio rerum eaque nisi totam labore aliquid perspiciatis mollitia sunt?</p>
@endsection