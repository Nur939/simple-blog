@extends("layouts.app")

@section("content")
    <h1>Edit Post</h1>

    {!!Form::open(["action" => ["PostsController@update",$post->id], "method"   =>  "POST","enctype" => "multipart/form-data"])!!}
        <div class="form-group">
            {{Form::label("title","Title")}}
            {{Form::text("title",$post->title,["class" => "form-control","placeholder" => "Title"])}}
        </div>
        <div class="form-group">
            {{Form::label("body","Body")}}
            {{Form::textarea("body",$post->body,["id" => "article-ckeditor", "class" => "form-control","placeholder" => "Body"])}}
        </div>
        <div class="form-group">
            {{Form::label("cover_image","Cover Image")}}
            <img style = "width:20%; height:20%;" src="/storage/cover_images/{{$post->cover_image}}" alt="">
            {{Form::file("cover_image")}}
        </div>
        <div class="form-group">
            {{Form::submit("Update",["class" => "btn btn-success"])}}
            {{Form::hidden("_method","PUT")}}
        </div>
    {!!Form::close()!!}
@endsection