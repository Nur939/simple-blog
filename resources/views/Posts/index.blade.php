@extends("layouts.app")

@section("content")
    @if(count($posts) > 0)
        <h1>All Posts</h1>
        @foreach($posts as $post)
            <div class="card card-body bg-light" style = "height:200px;">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                            <img style = "width:100%; height:50%;" src="/storage/cover_images/{{$post->cover_image}}" alt="">    
                    </div>
                    <div class="col-md-8 col-sm-8">
                        
                        <h2><a href = "/blog/{{$post->id}}">{{$post->title}}</a></h2>
                        <p>Written on {{$post->created_at}} by {{$post->user->name}}</p>
                    </div>
                </div>
            </div>
        @endforeach
        <span class = "float-right">{{$posts->links()}}</span>
    @else
        <p>No Posts Found</p>
    @endif    
@endsection