@extends("layouts.app")

@section("content")
    <a href="/blog" class = "btn btn-secondary">Go Back</a>
    <div class="card card-block">
        <h1>Post ID - {{$post->id}}</h1>
        <h2>Post Title - {{$post->title}}</h2>
        <img style = "width:50%; height:200px !important;" src="/storage/cover_images/{{$post->cover_image}}" alt="">
        <p>Post Body - {!!$post->body!!}</p>
        <p><small>Written on {{$post->created_at}} by {{$post->user->name}}</small></p>
        <p><small>Updated on {{$post->updated_at}} by {{$post->user->name}}</small></p>
    </div>

    @if(!Auth::guest())
        @if(Auth::user()->id == $post->user_id)
            <a href="/blog/{{$post->id}}/edit" class = "btn btn-primary">Edit</a>
            
            {!!Form::open(["action" =>  ["PostsController@destroy",$post->id],"method" => "POST", "class" => "float-right"])!!}
                {{Form::hidden("_method","Delete")}}
                {{Form::submit("Delete",["class" => "btn btn-danger"])}}
            {!!Form::close()!!}
        @endif
    @endif
@endsection