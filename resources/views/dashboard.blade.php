@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <p><a href="/blog/create" class = "btn btn-secondary">Create Post</a></p>
                    @if(count($posts) > 0)
                        <p>Your Blog Posts</p>
                        <table class = "table table-striped">
                            <tr>
                                <th>Title</th>
                                <th></th>
                                <th></th>
                            </tr>
                            @foreach($posts as $post)
                                <tr>
                                    <td>{{$post->title}}</td>
                                    <td><a href="/blog/{{$post->id}}/edit" class = "btn btn-success">Edit</a></td>
                                    <td>
                                        {!!Form::open(["action" => ["PostsController@destroy",$post->id],"method" => "POST", "class" => "float-right"])!!}
                                            {{Form::hidden("_method","DELETE")}}
                                            {{Form::submit("Delete",["class" => "btn btn-danger"])}}
                                        {!!Form::close()!!}
                                    </td>

                                </tr>
                            @endforeach
                        </table>
                    @else
                        <p>You have no posts</p>    
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
